import Xpath, {Xpaths} from "../src/xpath";
import WebElements from "../src/elements/webElements";
import WebAttribute from "../src/attributes/WebAttribute";
import FunctionsTest from "../src/functions/Functions";
import Functions from "../src/functions/Functions";
import Axes from "../src/axes/Axes";

class XpathTest {

	@Xpath({element: WebElements.Div, attribute: WebAttribute.DataQa, value: 'test', functions: FunctionsTest.Contains})
	private testElement: string;

	@Xpath({attribute: WebAttribute.Text, value: 'Example', functions: Functions.Contains})
	private exampleText: string;

	@Xpaths([{ attribute: WebAttribute.Text, value: 'Example', functions: Functions.Contains },
					{ attribute: WebAttribute.Text, value: 'Example', functions: Functions.Contains }])
	private exampleTexts: string;

	@Xpaths([{ attribute: WebAttribute.Text, value: 'Example', functions: Functions.Contains },
		       { attribute: WebAttribute.Text, value: 'Example', functions: Functions.Contains, axes: Axes.Child }])
	private exampleTextsAxes: string;

	run = () => {
		console.log('Running')
		console.log(this.testElement);
		console.log(this.exampleText)
		console.log(this.exampleTexts)
		console.log(this.exampleTextsAxes)

	}
}

new XpathTest().run()
