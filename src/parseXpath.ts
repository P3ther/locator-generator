import Elements from "./elements/elements";
import WebElements from "./elements/webElements";
import Attribute from "./attributes/Attribute";
import Functions from "../src/functions/Functions";
import WebAttribute from "./attributes/WebAttribute";
import Axes from "./axes/Axes";

export interface XpathInterface {
	element?: Elements | WebElements,
	attribute?: WebAttribute | Attribute,
	value?: string,
	functions?: Functions,
	axes?: Axes,
}

const parseXpath = (options: XpathInterface) => {
	let xpath: string = '//';

	if (options.axes) {
		xpath += `${options.axes}::`
	}

	if (options.element) {
		xpath += options.element
	} else {
		xpath += '*'
	}

	if (options.functions) {
		if(options.attribute && options.value) {
			xpath += `[${options.functions}(${options.attribute}, "${options.value}")]`
		} else if (options.attribute || options.value) {
			throw new Error('Xpath needs to contains both attribute and value.');
		}
	} else {
		if(options.attribute && options.value) {
			xpath += `[${options.attribute}="${options.value}"]`
		} else if (options.attribute || options.value) {
			throw new Error('Xpath needs to contains both attribute and value.');
		}
	}

	return xpath;
}

export default parseXpath;