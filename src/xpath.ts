import parseXpath, {XpathInterface} from "./parseXpath";

export function Xpath(xpathOptions: XpathInterface): PropertyDecorator {
	return function(target: any, options: any) {
		Object.defineProperty(target, options, {
			get: () => parseXpath(xpathOptions),
			set: () => {},
			enumerable: true,
			configurable: true
		});
	}
}

export function Xpaths(xpathOptions: XpathInterface[]): PropertyDecorator {
	console.log(xpathOptions)
	let xpath: string = '';
	xpathOptions.forEach((option) => {
		xpath += parseXpath(option);
	})

	return function(target: any, options: any) {
		Object.defineProperty(target, options, {
			get: () => xpath,
			set: () => {},
			enumerable: true,
			configurable: true
		});
	}
}

export default Xpath;