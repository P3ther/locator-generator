enum WebElements {
  Div = 'div',
  Span = 'span'
}

export default WebElements;