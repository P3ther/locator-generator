enum Functions {
	Contains = 'contains',
	Not = 'not',
}

export default Functions;