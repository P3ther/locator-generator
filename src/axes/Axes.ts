enum Axes {
	Child = 'child',
	Parent = 'parent',
	Self = 'self',
	Ancestor = 'ancestor',
	AncestorOrSelf = 'ancestor-or-self',
	Descendant = 'descendant',
	DescendantOrSelf = 'descendant-or-self',
	Following = 'following',
	FollowingSibling = 'following-sibling',
	Preceding = 'preceding',
	PrecedingSibling = 'preceding-sibling',
}

export default Axes;